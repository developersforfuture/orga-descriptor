###Organisation Repository
Actually, a better name is to be defined. The initial id was a catalogue, but a decentralized approach seems to be smarter.

##Objective
https://gitlab.com/developersforfuture/developersforfuture/issues/29

suggested to solve
https://trello.com/c/SLTneeLL (display forFuture locations on wechange) and https://trello.com/c/zzrj9HLg (vice versa) in a generic way

###Getting Started

#####Get the code
```
git clone ...
```

#####Build the project
```
mvn package
docker build --build-arg JAR_FILE=target/orgarepo-0.0.1-SNAPSHOT.jar -t orgarepo .
```

#####Run the image
```
docker run -p "8080:8080" orgarepo
```

Visit http://localhost:8080/

Using the checkbox below the map you can toggle is the location of the demo organisation are displayed.
