package net.parents4future.orgarepo.controller;


import net.parents4future.orgarepo.model.Location;
import net.parents4future.orgarepo.model.LocationListResource;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/api/randomlocations")
public class LocationsController {

    @GetMapping("/{count}")
    public LocationListResource getRandomLocations(@PathVariable(name = "count") int count) {
        List<Location> locations = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            Location location = new Location();
            location.setLat((float) getRandom(-90,90));
            location.setLon((float) getRandom(-180,180));
            location.setName("Location "+i);
            locations.add(location);
        }

        LocationListResource llr = new LocationListResource();
        llr.add(linkTo(LocationsController.class).slash(count).withSelfRel());
        llr.setLocationList(locations);
        return llr;
    }


    private double getRandom(int min, int max ){
        return (min + Math.random() * (max - min));
    }

}
