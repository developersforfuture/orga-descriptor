package net.parents4future.orgarepo.model;


import lombok.Data;
import org.springframework.hateoas.ResourceSupport;

import java.util.ArrayList;
import java.util.List;

@Data
public class LocationListResource extends ResourceSupport {
    private List<Location> locationList = new ArrayList<>();

    private String preferredIcon;
}
