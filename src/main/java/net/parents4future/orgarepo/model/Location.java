package net.parents4future.orgarepo.model;

import lombok.Data;

@Data
public class Location {
    String name;
    String url;
    Float lat;
    Float lon;
}
