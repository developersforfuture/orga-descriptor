package net.parents4future.orgarepo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OrgarepoApplication {
    public static void main(String[] args) {
        SpringApplication.run(OrgarepoApplication.class, args);
    }

}
