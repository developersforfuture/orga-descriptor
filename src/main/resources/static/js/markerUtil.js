function getMarkers(url, onLoadComplete) {
    var request = new XMLHttpRequest();

    request.open('GET', url, true);
    var markers = []
    request.onload = function () {
        // Begin accessing JSON data here
        var data = JSON.parse(this.response);
        var locations;
        if (data.locationList) locations = data.locationList;
        else locations = data._embedded.locationList;

        if (request.status >= 200 && request.status < 400) {
            locations.forEach(function (location) {
                markers.push(new google.maps.Marker({
                    position: new google.maps.LatLng(location.lat, location.lon),
                    title: location.name,
                    icon: data.preferredIcon
                }));

            });
            onLoadComplete(markers);
        } else {
            console.log('error')
        }
    };
    request.send();
}


function LocationsResource(name, url, map, display) {

    this.name = name;
    this.url = url;

    this.show = function () {
        this.markers.forEach(function (value) {
            value.setMap(map);
        });
    };

    this.hide = function () {
        this.markers.forEach(function (value) {
            value.setMap(null);
        });
    };

    this.setVisible = function (display){
        if (display) this.show();
        else this.hide();
    };


    var locRes=this;
    getMarkers(url, function (resultMarkers) {
        locRes.markers = resultMarkers;
        if (display) locRes.markers.forEach(function (value) {
            value.setMap(map);
        });
    });



}
